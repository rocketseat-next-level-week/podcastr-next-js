import { createContext, useState, ReactNode, useContext } from 'react'

type Episode = {
    title: string,
    members: string,
    thumbnail: string,
    duration: number,
    url: string
}

type PlayerContextData = {
    episodes: Array<Episode>,
    currentEpisodeIndex: number,
    isPlaying: boolean,
    isLooping: boolean
    isShuffling: boolean
    play: (episode: Episode) => void
    playList: (list: Array<Episode>, index: number) => void
    togglePlay: () => void
    toggleLoop: () => void
    toggleShuff: () => void
    playNext: () => void
    playPrevious: () => void
    setPlayingState: (state: boolean) => void
    clearPlayerState: () => void
    hasPrevious: boolean
    hasNext: boolean
}

type PlayerContextProviderProps = {
    children: ReactNode
}

export const PlayerContext = createContext({} as PlayerContextData)

export function PlayerContextProvider({ children }: PlayerContextProviderProps) {
    const [episodes, setEpisodes] = useState([])
    const [currentEpisodeIndex, setCurrentEpisodeIndex] = useState(0)
    const [isPlaying, setIsPlaying] = useState(false)
    const [isLooping, setIsLooping] = useState(false)
    const [isShuffling, setIsShuffling] = useState(false)

    const play = (episode: Episode) => {
        setEpisodes([episode])
        setCurrentEpisodeIndex(0)
        setIsPlaying(true)
    }

    const playList = (list: Array<Episode>, index: number) => {
        setEpisodes(list)
        setCurrentEpisodeIndex(index)
        setIsPlaying(true)
    }

    const hasPrevious = currentEpisodeIndex > 0
    const hasNext = isShuffling || (currentEpisodeIndex + 1) < episodes.length

    const playNext = () => {
        if (isShuffling) {
            const nextRandomEpisodeIndex = Math.floor(Math.random() * episodes.length)

            setCurrentEpisodeIndex(nextRandomEpisodeIndex)
        } else if (hasNext) {
            setCurrentEpisodeIndex(currentEpisodeIndex + 1)
        }
    }

    const playPrevious = () => {
        if (hasPrevious) {
            setCurrentEpisodeIndex(currentEpisodeIndex - 1)
        }
    }

    const togglePlay = () => setIsPlaying(!isPlaying)
    const toggleLoop = () => setIsLooping(!isLooping)
    const toggleShuff = () => setIsShuffling(!isShuffling)

    const setPlayingState = (state: boolean) => setIsPlaying(state) 

    const clearPlayerState = () => {
        setEpisodes([])
        setCurrentEpisodeIndex(0)
    }

    return (
        <PlayerContext.Provider
            value={{
                episodes,
                currentEpisodeIndex,
                isPlaying,
                isLooping,
                isShuffling,
                play,
                playList,
                togglePlay,
                toggleLoop,
                toggleShuff,
                playNext,
                playPrevious,
                setPlayingState,
                clearPlayerState,
                hasNext,
                hasPrevious
            }}
        >
            {children}
        </PlayerContext.Provider>
    )
}

export const usePlayer = () => {
    return useContext(PlayerContext)
}