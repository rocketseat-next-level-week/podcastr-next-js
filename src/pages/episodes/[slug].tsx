import { format, parseISO } from 'date-fns'
import ptBR from 'date-fns/locale/pt-BR'
import Image from 'next/image'
import Head from 'next/head'
import Link from 'next/link'
import { GetStaticPaths, GetStaticProps } from 'next'
import { api } from '../../services/api'
import { convertDurationToTimeString } from '../../utils/convertDurationToTimeString'
import { usePlayer } from '../../contexts/PlayerContext'

import styles from './episode.module.scss'

type Episode = {
    id: string,
    title: string,
    members: string,
    publishedAt: string,
    thumbnail: string,
    description: string,
    url: string,
    duration: number,
    formattedDuration: string
}

type EpisodeProps = {
    episode: Episode
}

export default function Episode ({ episode }: EpisodeProps) {
    const { title, thumbnail, members, publishedAt, formattedDuration, description } = episode

    const { play } = usePlayer()

    return (
        <div className={styles.episode}>
            <Head>
                <title>{title} | Podcastr</title>
            </Head>
            <div className={styles.thumbnailContainer}>
                <Link href="/">
                    <button type="button">
                        <img src="/arrow-left.svg" alt="Voltar"/>
                    </button>
                </Link>
                <Image
                    width={700}
                    height={160}
                    src={thumbnail}
                    objectFit="cover"
                />
                <button type="button">
                    <img src="/play.svg" alt="Tocar episódio" onClick={() => play(episode)}/>
                </button>
            </div>

            <header>
                <h1>{title}</h1>
                <span>{members}</span>
                <span>{publishedAt}</span>
                <span>{formattedDuration}</span>
            </header>

            <div className={styles.description} dangerouslySetInnerHTML={{ __html: description }}/>
        </div>
    )
}

const buildEpisode = ({
    id,
    title,
    members,
    published_at,
    thumbnail,
    description,
    file: {
      url,
      duration
    }
  }) => ({
    id,
    title,
    thumbnail,
    members,
    publishedAt: format(parseISO(published_at), 'd MMM yy', {
      locale: ptBR
    }),
    duration: Number(duration),
    formattedDuration: convertDurationToTimeString(Number(duration)),
    description,
    url
  })

export const getStaticPaths: GetStaticPaths = async () => {
    const { data } = await api.get('/episodes', {
        params: {
            _limit: 2,
            _sort: 'published_at',
            _order: 'desc'
        }
    })

    const paths = data.map(({ id }) => ({
        params: {
            slug: id
        }
    }))
    return {
        paths,
        fallback: 'blocking'
    }
}

export const getStaticProps: GetStaticProps = async (context) => {
    const { slug } = context.params

    const { data } = await api.get(`/episodes/${slug}`)

    return {
        props: {
            episode: buildEpisode(data)
        },
        revalidate: 60 * 60 * 24
    }
}