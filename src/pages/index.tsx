import { GetStaticProps } from 'next'
import Image from 'next/image'
import Head from 'next/head'
import Link from 'next/link'
import { format, parseISO } from 'date-fns'
import ptBR from 'date-fns/locale/pt-BR'
import { api } from '../services/api'
import { convertDurationToTimeString } from '../utils/convertDurationToTimeString'

import styles from './home.module.scss'
import { usePlayer } from '../contexts/PlayerContext'

type Episode = {
  id: string,
  title: string,
  members: string,
  publishedAt: string,
  thumbnail: string,
  description: string,
  url: string,
  duration: number,
  formattedDuration: string
}

type HomeProps = {
  allEpisodes: Array<Episode>
  latestEpisodes: Array<Episode>
}

export default function Home({ latestEpisodes, allEpisodes }: HomeProps) {
  const { playList } = usePlayer()

  const episodeList = [...latestEpisodes, ...allEpisodes]

  return (
    <div className={styles.homePage}>
      <Head>
        <title>Home | Podcastr</title>
      </Head>
      <section className={styles.latestEpisodes}>
        <h2>Últimos lançamentos</h2>

        <ul>
          {latestEpisodes
            .map((episode, index) => {
              const { id, thumbnail, title, members, publishedAt, formattedDuration } = episode

              return (
                <li key={id}>
                  <Image
                    width={192}
                    height={192}
                    src={thumbnail}
                    alt={title}
                    objectFit="cover"
                  />
  
                  <div className={styles.episodeDetails}>
                    <Link  href={`/episodes/${id}`}>
                      <a>{title}</a>
                    </Link>
                    <p>{members}</p>
                    <span>{publishedAt}</span>
                    <span>{formattedDuration}</span>
                  </div>
  
                  <button type="button" onClick={() => playList(episodeList, index)}>
                    <img src="/play-green.svg" alt="Tocar episódio" />
                  </button>
                </li>
              )
            })}
        </ul>
      </section>

      <section className={styles.allEpisodes}>
        <h2>Todos episódios</h2>

        <table cellSpacing={0}>
          <thead>
            <tr>
              <th></th>
              <th>Podcast</th>
              <th>Integrantes</th>
              <th>Data</th>
              <th>Duração</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {allEpisodes.map((episode, index) => {
              const { id, thumbnail, title, members, publishedAt, formattedDuration } = episode

              return (
                <tr key={id}>
                  <td style={{ width: 72 }}>
                    <Image
                      width={120}
                      height={120}
                      src={thumbnail}
                      alt={title}
                      objectFit="cover"
                    />
                  </td>
                  <td>
                    <Link  href={`/episodes/${id}`}>
                      <a>{title}</a>
                    </Link>
                  </td>
                  <td>{members}</td>
                  <td style={{ width: 100 }}>{publishedAt}</td>
                  <td>{formattedDuration}</td>
                  <td>
                    <button type="button" onClick={() => playList(episodeList, index + latestEpisodes.length)}>
                      <img src="/play-green.svg" alt="Tocar episódio" />
                    </button>
                  </td>
                </tr> 
              )
            })}
          </tbody>
        </table>
      </section>
    </div>
  )
}

// SPA
// useEffect(() => {
//   fetch('http://localhost:3333/episodes')
//     .then(response => response.json())
//     .then(console.log)
// }, [])

// SSR
// export async function getServerSideProps() {
//   const response = await fetch('http://localhost:3333/episodes')
//   const data = await response.json()
  
//   return {
//     props: { episodes: data }
//   }
// }

const buildEpisode = ({
  id,
  title,
  members,
  published_at,
  thumbnail,
  description,
  file: {
    url,
    duration
  }
}) => ({
  id,
  title,
  thumbnail,
  members,
  publishedAt: format(parseISO(published_at), 'd MMM yy', {
    locale: ptBR
  }),
  duration: Number(duration),
  formattedDuration: convertDurationToTimeString(Number(duration)),
  description,
  url
})
      
// SSG
export const getStaticProps: GetStaticProps = async () => {
  const { data } = await api.get('/episodes', {
    params: {
      _limit: 12,
      _sort: 'published_at',
      order: 'desc'
    }
  })

  const episodes = data.map(buildEpisode)
  const latestEpisodes = episodes.slice(0, 2)
  const allEpisodes = episodes.slice(2, episodes.lenth)
  
  return {
    props: { latestEpisodes, allEpisodes },
    revalidate: 60 * 60 * 8
  }
}
      